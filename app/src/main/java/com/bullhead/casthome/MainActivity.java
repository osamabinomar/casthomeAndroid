package com.bullhead.casthome;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.widget.EditText;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.bullhead.casthome.cast.CastManager;
import com.bullhead.casthome.cast.CastPlayer;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final EditText etUrl = findViewById(R.id.etLink);
        final CastPlayer player = CastManager.getInstance().getCastPlayer();

        findViewById(R.id.playButton)
                .setOnClickListener(v -> {
                    String url = etUrl.getText().toString();
                    if (!TextUtils.isEmpty(url)) {
                        if (CastManager.getInstance().isConnected()) {
                            player.play(url);
                        } else {
                            Snackbar.make(v, "Cast not connected.", Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        Snackbar.make(v, "Url is empty", Snackbar.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);
        CastButtonFactory.setUpMediaRouteButton(getApplicationContext(),
                menu,
                R.id.media_route_menu_item);

        return true;
    }
}
